class TestApiJoelsController < ApplicationController
  before_action :set_test_api_joel, only: [:show, :update, :destroy]

  # GET /test_api_joels
  def index
    @test_api_joels = TestApiJoel.all

    render json: @test_api_joels
  end

  # GET /test_api_joels/1
  def show
    render json: @test_api_joel
  end

  # POST /test_api_joels
  def create
    @test_api_joel = TestApiJoel.new(test_api_joel_params)

    if @test_api_joel.save
      render json: @test_api_joel, status: :created, location: @test_api_joel
    else
      render json: @test_api_joel.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /test_api_joels/1
  def update
    if @test_api_joel.update(test_api_joel_params)
      render json: @test_api_joel
    else
      render json: @test_api_joel.errors, status: :unprocessable_entity
    end
  end

  # DELETE /test_api_joels/1
  def destroy
    @test_api_joel.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_test_api_joel
      @test_api_joel = TestApiJoel.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def test_api_joel_params
      params.require(:test_api_joel).permit(:name, :description)
    end
end
