require 'test_helper'

class TestApiJoelsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @test_api_joel = test_api_joels(:one)
  end

  test "should get index" do
    get test_api_joels_url, as: :json
    assert_response :success
  end

  test "should create test_api_joel" do
    assert_difference('TestApiJoel.count') do
      post test_api_joels_url, params: { test_api_joel: { description: @test_api_joel.description, name: @test_api_joel.name } }, as: :json
    end

    assert_response 201
  end

  test "should show test_api_joel" do
    get test_api_joel_url(@test_api_joel), as: :json
    assert_response :success
  end

  test "should update test_api_joel" do
    patch test_api_joel_url(@test_api_joel), params: { test_api_joel: { description: @test_api_joel.description, name: @test_api_joel.name } }, as: :json
    assert_response 200
  end

  test "should destroy test_api_joel" do
    assert_difference('TestApiJoel.count', -1) do
      delete test_api_joel_url(@test_api_joel), as: :json
    end

    assert_response 204
  end
end
